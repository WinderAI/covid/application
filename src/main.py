import streamlit as st
import pages.home
import pages.cases
import pages.deaths

PAGES = {
    "Home": pages.home,
    "Cases": pages.cases,
    "Deaths": pages.deaths,
}


def main():
    """Main function of the App"""
    st.sidebar.title("Navigation")
    selection = st.sidebar.radio("Go to", list(PAGES.keys()))

    page = PAGES[selection]

    with st.spinner(f"Loading {selection} ..."):
        page.write()
    st.sidebar.title("Contribute")
    st.sidebar.info(
        """
        The [Athena Project](https://athena-project.life/) attempts to bring technologists together
        to help solve problems caused by COVID-19. Join us to help the fight!
        """
    )
    st.sidebar.title("About")
    st.sidebar.markdown(
        """
        This page is used by the #epidemic-model team to demonstrate new models.
        You can use this page to investigate COVID-19 data and predictions.

        This is sponsored by:
        """
    )
    st.sidebar.image("pages/images/winder_logo.png", use_column_width=True)


if __name__ == "__main__":
    main()
