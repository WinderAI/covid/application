"""Cases demo page"""
import datetime
import json
import urllib.request

import pandas as pd
import plotly.express as px
import plotly.graph_objects as go
import streamlit as st

base_url = "https://api.athena-project.life/v1/cases/confirmed/countries"


@st.cache
def get_data(country: str, from_date: datetime.datetime, to_date: datetime.datetime):
    dtf = datetime.datetime.combine(from_date, datetime.datetime.min.time())
    dtt = datetime.datetime.combine(to_date, datetime.datetime.min.time())
    url = f"{base_url}/{country}?time_from={dtf.isoformat()}&time_to={dtt.isoformat()}"
    req = urllib.request.Request(url, headers={"User-Agent": "Mozilla/5.0"})
    r = urllib.request.urlopen(req).read()
    data = json.loads(r.decode("utf-8"))
    df = pd.DataFrame(data["result"]).set_index("timestamp")
    return df


@st.cache
def get_countries():
    url = base_url
    req = urllib.request.Request(url, headers={"User-Agent": "Mozilla/5.0"})
    r = urllib.request.urlopen(req).read()
    data = json.loads(r.decode("utf-8"))
    return sorted(data)


def write():
    """Used to write the page in the app.py file"""
    with st.spinner("Loading Home ..."):
        countries = get_countries()
        country = st.selectbox("Select country:", countries)
        from_date = st.date_input("From", datetime.date(2020, 1, 1))
        to_date = st.date_input(
            "To", datetime.datetime.now() + datetime.timedelta(days=14)
        )

        df = get_data(country, from_date, to_date)
        fig = go.Figure()
        fig.add_trace(
            go.Scatter(
                x=df.index,
                y=df["credible_interval_low"],
                fill=None,
                mode="lines",
                line_color="rgba(0,0,0,0.0)",
                showlegend=False,
            )
        )
        fig.add_trace(
            go.Scatter(
                x=df.index,
                y=df["credible_interval_high"],
                fill="tonexty",  # fill area between this and previous trace
                mode="lines",
                line_color="rgba(0,0,0,0.0)",
                fillcolor="rgba(0,0,0,0.1)",
                name="95% credible interval",
            )
        )
        fig.add_trace(
            go.Scatter(
                x=df.index,
                y=df["median"],
                mode="lines+markers",
                name="Prediction (median)",
                line_color="black",
            )
        )
        fig.add_trace(
            go.Scatter(
                x=df.index,
                y=df["cumulative_cases"],
                mode="lines+markers",
                name="Confirmed",
                line_color="rgba(132,183,83,1.0)",
            )
        )

        st.plotly_chart(fig)
