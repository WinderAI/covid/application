# base image
FROM python:3.8

# exposing default port for streamlit
EXPOSE 5000

# copy over and install packages
COPY requirements.txt ./requirements.txt
RUN pip3 install -r requirements.txt

# copying everything over
COPY src .

# run app
ENTRYPOINT streamlit run main.py