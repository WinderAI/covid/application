"""Home page shown when the user enters the application"""
import streamlit as st

# pylint: disable=line-too-long
def write():
    """Used to write the page in the app.py file"""
    with st.spinner("Loading Home ..."):
        st.write(
            """
    # Home

    Welcome to the #epidemic-model team's demonstration application.

    Use the links on the left to browse to a specific demo.
    """
        )