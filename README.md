# Epidemic application

## Deploying

Any code committed to master will be deployed onto the production cluster. We intentionally do not have a test cluster to keep things simple. If you break it, you fix it.

### Build details

Code is automatically deployed by [Gitlab's AutoDevOps](https://docs.gitlab.com/ee/topics/autodevops/). It uses the [python buildpack](https://docs.gitlab.com/ee/topics/autodevops/stages.html#auto-build-using-heroku-buildpacks) with a [custom Dockerfile](https://docs.gitlab.com/ee/topics/autodevops/customize.html#custom-dockerfile). You can [customise](https://docs.gitlab.com/ee/topics/autodevops/customize.html) the deployment.

The application must be on port 5000 to work. Add to the `requirements.txt` to add new dependencies.

### Deploy details

The built container is deployed with a generic helm template. You can [customize the helm chart](https://docs.gitlab.com/ee/topics/autodevops/customize.html#custom-helm-chart) if you need to.

### Database

AutoDevOps deploys a [Postgres database](https://docs.gitlab.com/ee/topics/autodevops/customize.html#postgresql-database-support) by default. If you need to store data/parameters, it makes sense to take advantage of that.

### Secrets and more

Extra information in [their documentation](https://docs.gitlab.com/ee/topics/autodevops/customize.html).


## Build

```
docker build -t test .
```

## Run locally

```
docker run -it -p 5000:5000 test
open http://localhost:5000
```